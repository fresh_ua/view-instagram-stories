<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 23.03.18
 * Time: 09:06
 */

require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$api = new \InstagramAPI\Instagram();

$loginResponse = $api->login(getenv('LOGIN'), getenv('PASSWORD'));
$stories = $api->story->getUserReelMediaFeed(getenv('STORIES_USER_ID'));

$seenResponse = $api->story->markMediaSeen($stories->getItems());

if ($seenResponse->isOk()) {
    exit("All stories mark as seen".PHP_EOL);
}
else {
    exit("Error".PHP_EOL);
}